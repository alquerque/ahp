import numpy as _np


def create_matrix_from_weights(weights, dtype=_np.float32):
    n = len(weights)
    a = _np.zeros((n, n), dtype=dtype)
    for i in range(n):
        for j in range(n):
            a[j, i] = weights[i] / weights[j]
    return a


def r_i(n):
    table = [0, 0, .58, .90, 1.12, 1.24, 1.32, 1.41, 1.45, 1.49]
    assert (isinstance(n, int))
    assert (n > 0)
    assert (n < 11)
    return table[n - 1]


def extract_weights(a, precision=0.001, max_iter=10):
    n = a.shape[0]
    w_k_1 = a.prod(axis=0) ** (1 / a.shape[0])
    w_k_1 /= w_k_1.sum()
    a_k = a
    w_k = _np.zeros_like(w_k_1)
    c_i = 0
    for k in range(2, max_iter + 1):
        w_k = a_k @ w_k_1
        lambda_max = _np.sum(w_k)
        w_k /= lambda_max
        c_i = (lambda_max - n) / (n - 1)
        difference = abs(w_k_1 - w_k).sum()

        if difference < precision:
            return w_k, c_i / r_i(n), True
        else:
            w_k_1 = w_k.copy()
    else:
        return w_k, c_i / r_i(n), False
