import unittest

import numpy as np
import numpy.testing as npt

import ahp.weights as aw


class TestWeights(unittest.TestCase):
    def test_matrix_generation_from_weights(self):
        weights = [1, 2]
        mat = aw.create_matrix_from_weights(weights)
        expected_result = np.array([[1, 2], [1 / 2, 1]])
        npt.assert_array_almost_equal(expected_result, mat)

    def test_extract_weights(self):
        a = np.array([[1, 1 / 7, 1 / 5, 1 / 5], [7, 1, 2, 3], [5, 1 / 2, 1, 1],
                      [5, 1 / 3, 1, 1]])
        w, c_i, converged = aw.extract_weights(a)
        expected_result = np.array([0.053, 0.491, 0.238, 0.213])
        expected_inconsistency = 0.02
        self.assertTrue(converged)
        npt.assert_array_almost_equal(expected_result, w, decimal=2)
        npt.assert_almost_equal(c_i, expected_inconsistency, decimal=2)


    def test_extract_weights_second(self):
        a = np.array([[1, 1 / 4, 1 / 3, 1 / 3, 7],
                      [4, 1, 2, 3, 7],
                      [3, 1 / 2, 1, 3, 6],
                      [3, 1 / 3, 1 / 3, 1, 4],
                      [1 / 7, 1 / 7, 1 / 6, 1 / 4, 1]])

        w, c_i, converged = aw.extract_weights(a)
        expected_result = np.array([0.115, 0.402, 0.283, 0.163, 0.037])
        expected_inconsistency = 0.092
        self.assertTrue(converged)
        npt.assert_array_almost_equal(expected_result, w, decimal=2)
        npt.assert_almost_equal(c_i, expected_inconsistency, decimal=3)
